<?php

/**
 * Provides an admin form.
 */
function key_authentication_admin($form, &$form_state) {
  $form['auto_generate_keys'] = array(
    '#type' => 'checkbox',
    '#title' => t('Automatically generate a key when users are created'),
    '#default_value' => variable_get('key_authentication_auto_generate_keys', TRUE),
    '#description' => t('This applies only to new users that have access to use key authentication.'),
  );

  $form['key_authentication_key_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Key length'),
    '#default_value' => variable_get('key_authentication_key_length', 32),
    '#required' => TRUE,
    '#description' => t('Existing keys will not be affected.'),
    '#size' => 10,
    '#element_validate' => array('element_validate_integer_positive'),
  );

  $form['key_authentication_param_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Parameter name'),
    '#default_value' => variable_get('key_authentication_param_name', 'api-key'),
    '#required' => TRUE,
    '#description' => t('The name of the parameter used to send the API key via one of the selected detection methods below.'),
  );

  $form['key_authentication_detection_methods'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Detection methods'),
    '#default_value' => variable_get('key_authentication_detection_methods', array('header', 'query')),
    '#required' => TRUE,
    '#options' => [
      'header' => t('Header'),
      'query' => t('Query'),
    ],
    '#description' => t('Select one or more methods of detecting the API key.'),
  );

  return system_settings_form($form);
}
