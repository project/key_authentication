<?php

/**
 * Provides a form to manage the user's key for authentication.
 */
function key_authentication_user_key_auth_form($form, &$form_state, $user) {
  $uid = $user->uid;

  // Extract the user's key.
  $key = key_authentication_get_key_by_uid($uid);

  // Store the user ID.
  $form['#uid'] = $uid;

  $form['key'] = array(
    'label' => array(
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => t('Key'),
    ),
    'key' => array(
      '#type' => 'item',
      '#markup' => $key ? $key : t('You currently do not have a key'),
    ),
  );

  $form['auth'] = array(
    'label' => array(
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => t('Authentication options'),
    ),
    '#access' => (bool) $key,
  );

  if (in_array('header', variable_get('key_authentication_detection_methods', array('header', 'query')))) {
    $form['auth']['header'] = array(
      'label' => array(
        '#type' => 'html_tag',
        '#tag' => 'h5',
        '#value' => t('Header'),
      ),
      'instructions' => array(
        '#type' => 'item',
        '#markup' => t('Include the following header in your API requests.'),
      ),
      'example' => array(
        '#type' => 'html_tag',
        '#tag' => 'pre',
        '#value' => variable_get('key_authentication_param_name', 'api-key') . ': ' . $key,
      ),
    );
  }

  if (in_array('query', variable_get('key_authentication_detection_methods', array('header', 'query')))) {
    $form['auth']['query'] = array(
      'label' => array(
        '#type' => 'html_tag',
        '#tag' => 'h5',
        '#value' => t('Query'),
      ),
      'instructions' => array(
        '#type' => 'item',
        '#markup' => t('Include the following query in the URL of your API requests.'),
      ),
      'example' => array(
        '#type' => 'html_tag',
        '#tag' => 'pre',
        '#value' => variable_get('key_authentication_param_name', 'api-key') . ': ' . $key,
      ),
    );
  }

  $form['actions'] = array(
    'new' => array(
      '#type' => 'submit',
      '#value' => t('Generate new key'),
    ),
    'delete' => array(
      '#type' => 'submit',
      '#value' => t('Delete current key'),
      '#access' => (bool) $key,
      '#submit' => array('key_authentication_user_key_auth_form_delete_key'),
    ),
  );

  return $form;
}

/**
 * Provides a submit form.
 */
function key_authentication_user_key_auth_form_submit($form, &$form_state) {
  $uid = $form['#uid'];
  // Generate a new key.
  db_merge('authmap')
    ->fields(array(
      'uid' => $uid,
      'authname' => key_authentication_generate_key(),
      'module' => 'key_authentication',
    ))
    ->key(array('uid' => $uid, 'module' => 'key_authentication'))
    ->execute();

  // Alert the user.
  drupal_set_message(t('A new key has been generated.'));
}

/**
 * Provides a delete key.
 */
function key_authentication_user_key_auth_form_delete_key($form, &$form_state) {
  // Delete the key.
  db_delete('authmap')
    ->condition('uid', $form['#uid'])
    ->condition('module', 'key_authentication')
    ->execute();

  // Alert the user.
  drupal_set_message(t('Your key has been deleted.'));
}
